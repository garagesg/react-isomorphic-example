import alt from '../alt';

class HomeActions {
  constructor() {
    this.generateActions(
      'setStatus'
    );
  }

  setStatus(status) {
    this.dispatch(status);
  }
}

export default alt.createActions(HomeActions);