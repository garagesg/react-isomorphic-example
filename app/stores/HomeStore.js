import alt from '../alt';
import HomeActions from '../actions/HomeActions';

class HomeStore {
  constructor() {
    this.status = false;
    this.bindActions(HomeActions);
  }

  setStatus(status) {
    this.status = status; 
  }
}

export default alt.createStore(HomeStore);