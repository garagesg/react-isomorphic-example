import React from 'react';
import { Route, NotFoundRoute, DefaultRoute, Redirect } from 'react-router';

import App from './components/App';
import Home from './components/Home';
import About from './components/About';
import NotFound from './components/NotFound';


const Routes = [
  <Route path="/" name="app" handler={App}>    
    <DefaultRoute name="home" handler={Home} />
    <Route name="about" handler={About} />
  </Route>,
  <NotFoundRoute name="notFound" handler={NotFound} />
];

export default Routes;