/*****************************
  BREAKPOINTS (keep in sync with variables.less)
******************************/
exports.breakpointMobile = 599; //max-width
exports.breakpointTablet =  1023; //max-width
exports.breakpointDesktop =  1439; //max-width
exports.breakpointDesktopXL = 1440; //min-width

/*****************************
  MEDIA QUERIES (keep in sync with variables.less)
******************************/
exports.breakpointQueryRetina = '(-webkit-min-device-pixel-ratio: 1.5),(min--moz-device-pixel-ratio: 1.5),(-o-min-device-pixel-ratio: 3/2),(min-device-pixel-ratio: 1.5)';
exports.breakpointQueryMobile = "(max-width: 599px)";
exports.breakpointQueryTablet = "(min-width: 600px) and (max-width: 1023px)";
exports.breakpointQueryDesktop = "(min-width: 1024px) and (max-width: 1439px)";
exports.breakpointQueryDesktopXL = "(min-width: 1440px)";

exports.isClient = typeof window != 'undefined';

if(exports.isClient){
  exports.url = '/api/';
}else{
  exports.url = 'http://localhost:3000/api/';
}