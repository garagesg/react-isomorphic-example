import React from 'react';
import { Link } from 'react-router';

const Navbar = React.createClass({
  displayName: 'Navbar',

  render() {

    const {navItems} = this.props;

    return (
      <div className="Navbar">
       {navItems.map(navItem =>
          <Link 
            className="Navbar-item"
            to={navItem.to}
            title={navItem.name}
            key={navItem.name}
          >
            {navItem.name}
          </Link>
        )}
      </div>
    );
  }
});

export default Navbar;