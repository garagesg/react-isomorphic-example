var React = require('react');

var NotFound = React.createClass({
  displayName: "NotFound",
  
  render() {
    return (
      <div>
        404 Page Not Found
      </div>
    );
  }
})

module.exports = NotFound