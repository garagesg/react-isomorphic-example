var React = require('react');
var Link = require('react-router').Link;
var Header = require('./Header');
var HeroImage = require('./shared/HeroImage');

var Example = React.createClass({
  displayName: "Example",

  componentDidMount() {

  },

  _onResize() {

  },

  render() {
    return (
      <div>
        <Header />
        <div className='home' ref="Home">
          <HeroImage sizes={
            {
              "s":"http://placehold.it/600x600, http://placehold.it/1200x1200 2x",
              "m":"http://placehold.it/1024x1024, http://placehold.it/2048x2048 2x",
              "l":"http://placehold.it/1440x1440",
              "xl":"http://placehold.it/2048x2048",
            }
          } />
        </div>
      </div>
    );
  }
})

module.exports = Example