'use strict';

import React from'react';
import constants from'../constants';
import superagent from'superagent-ls';
import { canUseDOM } from 'react/lib/ExecutionEnvironment';

import Navbar from './Navbar';
import ResponsiveImage from './utils/ResponsiveImage';

let _logoImg = {
  "img": {
    "s": "http://placehold.it/160x160, http://placehold.it/320x320 2x",
    "m": "http://placehold.it/320x320, http://placehold.it/640x640 2x",
    "l": "http://placehold.it/640x640, http://placehold.it/1280x1280 2x",
    "xl": "http://placehold.it/640x640, http://placehold.it/1280x1280 2x"
  }
};

const About = React.createClass({
  displayName: 'About',

  statics: {
    fetchData(callback) {

      let _url = constants.url+'about.json';

      superagent.get(_url).accept('json').end((err, res) => {
        callback(err, res && {
          title: res.body.meta.title,
          description: res.body.meta.description, 
          image: res.body.meta.image, 
          about: res.body.about, 
          nav: res.body.nav
        });
      });
    }
  },

  componentDidMount() {
    
  },

  render() {
    const { about,nav } = this.props.data;
    return (
      <div>
        <Navbar {...nav} />
        <div className='about' ref="About">
          <h1>{about.title}</h1>
          <p>{about.description}</p>
        </div>
        <ResponsiveImage 
          sizes={_logoImg.img}
          alt="Test ResponsiveImage" 
          noPreload={true}
        />
      </div>
    );
  }
})

export default About;