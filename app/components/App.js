'use strict';

import React from 'react';
import { RouteHandler } from 'react-router';

const App = React.createClass({
  displayName: 'App',
  statics: {
    title: 'React Isomorphic Example App'
  },

  render() {
    const handler = <RouteHandler {...this.props} />;
      
    return (
      <div>
        <div className="App">
          {handler}
        </div>
      </div>
    )
  }  
});

export default App;