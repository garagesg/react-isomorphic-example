'use strict';

import React from 'react';
import { canUseDOM } from 'react/lib/ExecutionEnvironment';
import constants from '../../constants';

let { PropTypes } = React;
let pictureEl;

if (canUseDOM) {
  let picturefill = require('picturefill');
  let lazysizes = require('lazysizes');
}

const ResponsiveImage = React.createClass({
  displayName: 'ResponsiveImage',
  propTypes: {
    alt: PropTypes.string,
    ClassName: PropTypes.string,
    sizes: PropTypes.object,
    key: PropTypes.string
  },

  getInitialState() {
    return {
      imgSrc: ''
    };
  },

  componentDidMount() {
    picturefill();
    pictureEl = React.findDOMNode(this.refs.picture);
  },

  render() {
    const sizes = this.props.sizes;

    if (!sizes) {
      console.warn('ResponsiveImage requires sizes prop set. `props.sizes = { s|m|l|xl: {w:Number, h:Number} }`.');
    }

    const sources = 
      Object
        .keys(sizes)
        .map(sizeKey => {
          const size = sizes[sizeKey];
          let media = '(max-width: ' + constants.breakpointMobile + 'px)';

          if(sizeKey === 'm') {
            media = '(max-width: ' + constants.breakpointTablet + 'px)';
          }
          else if(sizeKey === 'l') {
            media = '(max-width: ' + constants.breakpointDesktop + 'px)';
          }
          else if(sizeKey === 'xl') {
            media = '(min-width: ' + constants.breakpointDesktopXL + 'px)';
          }

          const source = this.props.noPreload ?
            <source srcSet={size} media={media} key={media} /> :
            <source data-srcset={size} media={media} key={media} />

          return source;
        });

    let lowSource = sizes.s;

    if(sizes.s && sizes.s.split(',').length > 0) {
      lowSource = sizes.s.split(',')[0]
    }

    const imageLoader = (
      <img 
        data-src={lowSource} // Add this for ie9
        className="lazyload"
        alt={this.props.alt} 
        ref="ResponsiveImage"
      />
    );
    
    return (
      <picture 
        className={this.props.ClassName}
        style={this.props.bgStyle}
        ref="picture"
        key={lowSource}
      >
        {sources}
        {imageLoader}
      </picture>
    );
  }
});

export default ResponsiveImage;