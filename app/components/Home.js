'use strict';

import React from'react';
import constants from '../constants';
import superagent from 'superagent-ls';
import { canUseDOM } from 'react/lib/ExecutionEnvironment';

import HomeStore from '../stores/HomeStore';
import HomeActions from '../actions/HomeActions';

import Navbar from './Navbar';

const Home = React.createClass({
  displayName: 'Home',

  statics: {
    fetchData(callback) {

      let _url = constants.url+'home.json';

      superagent.get(_url).accept('json').end((err, res) => {
        callback(err, res && {
          title: res.body.meta.title,
          description: res.body.meta.description, 
          image: res.body.meta.image, 
          homepage: res.body.homepage, 
          nav: res.body.nav
        });
      });
    }
  },

  onChange(state) {
    this.setState(state);
  },

  componentWillMount() {
    this.setState(HomeStore.getState());
  },

  componentDidMount() {
    HomeStore.listen(this.onChange);
  },

  componentWillUnmount() {
    HomeStore.unlisten(this.onChange);
  },

  _toggleStatus() {
    HomeActions.setStatus( this.state.status ? false : true );
  },

  render() {
    const { homepage,nav } = this.props.data;
    return (
      <div>
        <Navbar {...nav} />
        <div className='home' ref="Home">
          <h1>{homepage.title}</h1>
          <p>{homepage.description}</p>
        </div>
        <a className={this.state.status ? 'StatusToggle active' : 'StatusToggle'} onClick={this._toggleStatus}>
          Status: {this.state.status ? 'true' : 'false'}
        </a>
      </div>
    );
  }
})

export default Home;