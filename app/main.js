require('setimmediate')

import React from 'react';
import Router from 'react-router';
import routes from './routes';

import assign from 'react/lib/Object.assign';
import fetchData from '../utils/fetchData';
import getTitle from '../utils/getTitle';

const appDiv = document.getElementById('app');

Router.run(routes, Router.HistoryLocation, (Handler, state) => {
  fetchData(state.routes, state.params, (err, fetchedData) => {
    const props = assign({}, fetchedData, state.data);
    React.render(<Handler {...props}/>, appDiv);
    document.title = getTitle(state.routes, state.params, props);
  });
});