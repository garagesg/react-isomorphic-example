'use strict';

var React = require('react')
var assign = require('react/lib/Object.assign')
var Router = require('react-router')
var Redirect = require('react-router/lib/Redirect')
var constants = require('constants');

var swig  = require('swig');
var {StaticLocation} = Router

var fetchData = require('./utils/fetchData')
var getTitle = require('./utils/getTitle') 
var getDescription = require('./utils/getDescription')


module.exports = function(routes, options) {
  if (!routes) {
    throw new Error('Routes must be provided')
  }
  options = assign({title: {}}, options)

  function renderApp(location, cb) {
    var router = Router.create({
      location,
      routes,

      onAbort(reason) {
        if (reason instanceof Error) {
          cb(reason)
        }
        else if (reason instanceof Redirect) {
          cb(null, router, {redirect: reason})
        }
        else {
          cb(null, router, reason)
        }
      },

      onError(err) {
        console.log('error',err)
        cb(err)
      }
    })

    router.run((Handler, state) => {

      if (state.routes[0].name == 'notFound') {

        fetchData(state.routes, state.params, (err, fetchedData) => {
          var props = assign({}, fetchedData, state.data)
          var html = React.renderToString(<Handler {...props}/>)
          var title = 'Default Title goes here';
          var description = '';
          var image = '';

          if(fetchedData.data != undefined){
            title = getTitle(state.routes, state.params, props, options.title);
            description = fetchedData.data.description;
            image = fetchedData.data.image;
          }
          return cb(null, router, {notFound: true}, html, JSON.stringify(props), title, description, image)
        })

      }else{

        fetchData(state.routes, state.params, (err, fetchedData) => {
          var props = assign({}, fetchedData, state.data)
          var html = React.renderToString(<Handler {...props}/>)
          var title = 'Default Title goes here';
          var description = '';
          var image = '';

          if(fetchedData.data != undefined){
            title = getTitle(state.routes, state.params, props, options.title);
            description = fetchedData.data.description;
            image = fetchedData.data.image;
          }

          cb(null, router, null, html, JSON.stringify(props), title, description, image)
        })

      }
    })
  }

  function renderAppHandler(res, next, err, router, special, html, props, title, description, image) { 

    if (err) {
      return next(err)
    }

    if (special === null) {

      var page = swig.renderFile('views/index.html',  {
        title: title, 
        description: description,
        image: image,
        html: html,
        props: props});

      return res.send(page);
    }

    if (special.notFound) {

      var page = swig.renderFile('views/index.html',  {
        title: title, 
        description: description,
        image: image,
        html: html,
        props: props});

      return res.status(404).send(page);

    }
    else if (special.redirect) {
      var redirect = special.redirect
      var path = router.makePath(redirect.to, redirect.params, redirect.query)
      // Rather than introducing a server-specific abort reason object, use the
      // fact that a redirect has a data property as an indication that a
      // response should be rendered directly.
      if (redirect.data) {
        renderApp(
          new StaticLocation(path, redirect.data),
          renderAppHandler.bind(null, res, next)
        )
      }
      else {
        res.redirect(303, path)
      }
    }
    else {
      console.error('Unexpected special response case: ', special.constructor, special)
      next(new Error('Unexpected special response case, see server logs.'))
    }
  }

  return function reactRouter(req, res, next) {
    // Provide the method and body of non-GET requests as a request-like object
    var data = null
    if (req.method != 'GET') {
      data = {method: req.method, body: req.body}
    }
    var location = new StaticLocation(req.url, data)
    renderApp(location, renderAppHandler.bind(null, res, next))
  }
}