# SETUP

install node version 0.12.5

# List of packages and description
| Package Name    | Description|
| -------------   |:-------------: |
| alt         | Flux library for React.|
| async         | For managing asynchronous flow.|
| babel         | ES6 compiler.|
| body-parser     | For parsing POST request data.|
| colors        | Pretty console output messages.|
| compression     | Gzip compression.|
| express       | Web framework for Node.js.|
| morgan        | HTTP request logger.|
| react         | React.|
| react-router    | Routing library for React.|
| request       | For making HTTP requests to API.|
| serve-favicon   | For serving favicon.png icon.|
| socket.io       | To display how many users are online in real-time.|
| swig        | To render the initial HTML template.|
| underscore      | Helper JavaScript utilities.|

Run __npm install__ in the Terminal to install the packages that we specified in the package.json.

nodemon is a node module that needs to be installed with -g.

use "npm run watch" for dev environment

use "gulp" for dev environment

---

# Serverside Rendering

React does serverside rendering and most of the 'magic' can be found in the 'react-router-middleware.js' file.

Superagent is used to load each page's JSON and automatically will assign the page title, description and other meta tags.

---

# LINT

eslint https://github.com/eslint/eslint  
eslint-plugin-react https://github.com/yannickcr/eslint-plugin-react  
babel-eslint https://github.com/babel/babel-eslint  
Sublime Text lint settings https://medium.com/@dan_abramov/lint-like-it-s-2015-6987d44c5b48  

---

# Patterns

Method Organization http://reactjsnews.com/react-style-guide-patterns-i-like/  
Structure etc https://github.com/kriasoft/react-starter-kit/blob/master/docs/react-style-guide.md

---

# Libraries in use

React - https://facebook.github.io/react/  
React is a isomorphic javascript library that Facebook created. Its component based structure and isomorphic features was the perfect choice for the technical considerations for ClashOfClans.com.

React Router - https://github.com/rackt/react-router/tree/0.13.x  
A complete routing library for React. React Router was perfect for isomorphic js. We kept with version 0.13.x as 1.00 was in beta when we started the project and was actively being developed.

Alt - https://github.com/goatslacker/alt  
Alt is a Flux library. Flux is the application architecture that Facebook uses for building client-side web applications. 

Express - http://expressjs.com/  
Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications. We are using Express as the server for our React website. 

Superagent- https://visionmedia.github.io/superagent/  
For AJAX requests both on server and client side.

Babel - https://babeljs.io/  
Babel compiles your ES6 javascript into ES5 javascript.

LessCSS - http://lesscss.org/  

Swig - http://paularmstrong.github.io/swig/  
Simple templating engine.

Picturefill - https://github.com/scottjehl/picturefill  
For Cross Browser Responsive Images 

---

The base structure was based on the code samples found on: http://sahatyalkabov.com/create-a-character-voting-app-using-react-nodejs-mongodb-and-socketio/