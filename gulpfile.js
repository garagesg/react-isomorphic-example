var gulp = require('gulp');
var gutil = require('gulp-util');
var gulpif = require('gulp-if');
var streamify = require('gulp-streamify');
var autoprefixer = require('gulp-autoprefixer');
var cssmin = require('gulp-cssmin');
var less = require('gulp-less');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var source = require('vinyl-source-stream');
var babelify = require('babelify');
var babel = require('gulp-babel');
var browserify = require('browserify');
var watchify = require('watchify');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync').create();
var nodemon = require('gulp-nodemon');
var eslint = require('gulp-eslint');
var jsonlint = require('gulp-jsonlint');
var notifier = require('node-notifier');
var gulpSequence = require('gulp-sequence');
var htmlreplace = require('gulp-html-replace');
var colors = gutil.colors;

// Creating a Fake Environment to avoid troubles 
// with process.env.NODE_ENV
var NODE_ENV = process.env.NODE_ENV;

// Detects if the Fake environment is production
// This is done to minify the app in production
function isProduction() {
  return process.env.NODE_ENV === 'production';
}

var dependencies = [
  'alt',
  'react',
  'react/lib/ReactTransitionGroup',
  'react-router',
  'run-parallel'
];

/*
 |--------------------------------------------------------------------------
 | Copies JSON files into public folder
 |--------------------------------------------------------------------------
 */
gulp.task('copy', function() {
  gulp.src('./app/api/**/*')
    .pipe(gulp.dest('./public/api'));

  gulp.src('./public/**/*')
    .pipe(gulp.dest('./dist/public'));
});

/*
 |--------------------------------------------------------------------------
 | ES LINT
 |--------------------------------------------------------------------------
 */
gulp.task('lint', function () {
    return gulp.src(['./app/**/*.js'])
      // eslint() attaches the lint output to the eslint property
      // of the file object so it can be used by other modules.
      .pipe(eslint({
          useEslintrc: true
      }))
      // eslint.format() outputs the lint results to the console.
      // Alternatively use eslint.formatEach() (see Docs).
      .pipe(eslint.format())
      // To have the process exit with an error code (1) on
      // lint error, return the stream and pipe to failOnError last.
      .pipe(eslint.failOnError());
});

/*
 |--------------------------------------------------------------------------
 | Babel compile server side and client side code
 |--------------------------------------------------------------------------
 */
gulp.task('babel', function() {
  return gulp.src(['server.js','react-router-middleware.js','app/**/*.js','utils/**/*'], {
        base: './'
    })
    .pipe(babel())
    .pipe(gulp.dest('dist'));
});

/*
 |--------------------------------------------------------------------------
 | Compile third-party dependencies separately for faster performance.
 |--------------------------------------------------------------------------
 */
gulp.task('browserify-vendor', function() {
  return browserify()
    .require(dependencies)
    .bundle()
    .pipe(source('vendor.bundle.js'))
    .pipe(gulpif(isProduction, streamify(uglify({ mangle: false, compress: true }))))
    .pipe(gulp.dest('public/js'));
});

/*
 |--------------------------------------------------------------------------
 | Compile only project files, excluding all third-party dependencies.
 |--------------------------------------------------------------------------
 */
gulp.task('browserify', ['browserify-vendor'], function() {
  return browserify('app/main.js')
    .external(dependencies)
    .transform(babelify)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulpif(isProduction, streamify(uglify({ mangle: false, compress: true }))))
    .pipe(gulp.dest('public/js'));
});

/*
 |--------------------------------------------------------------------------
 | Combine the bundles together for production
 |--------------------------------------------------------------------------
 */
gulp.task('concatScripts', function() {
  return gulp.src([
    'public/js/vendor.bundle.js',
    'public/js/bundle.js'
  ])
  .pipe(concat('scripts.min.js'))
  .pipe(streamify(uglify({ mangle: false, compress: true })))
  .pipe(gulp.dest('public/js'));
});

/*
 |--------------------------------------------------------------------------
 | Combine the bundles together for production
 |--------------------------------------------------------------------------
 */
gulp.task('htmlReplace', function() {
  return gulp.src('views/index.html')
    .pipe(htmlreplace({
      js: {
        src: '{{path}}/js/scripts.min.js?cache={{cache}}',
        tpl: '<script async src="%s"></script>'        
      }
    }))
    .pipe(gulp.dest('dist/views'));
});

/*
 |--------------------------------------------------------------------------
 | Same as browserify task, but will also watch for changes and re-compile.
 |--------------------------------------------------------------------------
 */
gulp.task('browserify-watch', ['browserify-vendor'], function() {
  var bundler = watchify(browserify('app/main.js', watchify.args));
  bundler.external(dependencies);
  bundler.transform(babelify);
  bundler.on('update', rebundle);
  return rebundle();

  function rebundle() {
    var start = Date.now();
    return bundler.bundle()
      .on('error', function(err) {
        gutil.log(gutil.colors.red(err.toString()));
      })
      .on('end', function() {
        gutil.log(gutil.colors.green('Finished rebundling in', (Date.now() - start) + 'ms.'));
      })
      .pipe(source('bundle.js'))
      .pipe(gulp.dest('public/js/'));
  }
});

/*
 |--------------------------------------------------------------------------
 | Live reloading for dev environment
 |--------------------------------------------------------------------------
 */
gulp.task('browser-sync',  function() {
  browserSync.init(null, {
    proxy: "http://localhost:3000",
        files: ["app/**/*.*"],
        browser: "google chrome",
        port: 3001
  });
});

gulp.task('nodemon', function (cb) {
  
  var started = false;
  
  return nodemon({
    script: 'server.js'
  }).on('start', function () {
    // to avoid nodemon being started multiple times
    // thanks @matthisk
    if (!started) {
      cb();
      started = true; 
    } 
  });
});

/*
 |--------------------------------------------------------------------------
 | Reporting error on JSON files
 |--------------------------------------------------------------------------
 */
gulp.task('jsonlint', function() {
  return gulp.src('app/api/*.json')
    .pipe(jsonlint())
    .pipe(jsonlint.reporter(function(file) {
      notifier.notify({
        'title': 'JSON Error:',
        'message': file.jsonlint.message
      });
      gutil.log(colors.yellow('Error on file ') + colors.magenta(file.path));
      gutil.log(colors.red(file.jsonlint.message));
    }));
});

/*
 |--------------------------------------------------------------------------
 | Compile LESS stylesheets.
 |--------------------------------------------------------------------------
 */
gulp.task('styles', function() {
  return gulp.src('app/stylesheets/main.less')
    .pipe(plumber())
    .pipe(less())
    .pipe(autoprefixer())
    .pipe(gulpif(isProduction, cssmin()))
    .pipe(gulp.dest('public/css'));
});

/*
 |--------------------------------------------------------------------------
 | Set Node Production Environment
 |    Also, store the current NODE_ENV
 |--------------------------------------------------------------------------
 */
gulp.task('setProduction', function() {
  NODE_ENV = process.env.NODE_ENV;
  return process.env.NODE_ENV = 'production';
});

/*
 |--------------------------------------------------------------------------
 | Restore Node Environment
 |--------------------------------------------------------------------------
 */
gulp.task('restoreEnv', function() {
  return process.env.NODE_ENV = NODE_ENV;
});

/*
 |--------------------------------------------------------------------------
 | Watch Stuff
 |--------------------------------------------------------------------------
 */
gulp.task('watch', function() {
  gulp.watch('app/stylesheets/**/*.less', ['styles']);
  gulp.watch('app/**/*.json', ['jsonlint']);
  gulp.watch('app/**/*');
  gulp.watch('app/api/**/*',['copy']);
});

gulp.task('default', ['styles', 'copy', 'browserify-watch', 'watch','browser-sync']);
gulp.task('build', gulpSequence(['setProduction'], ['styles', 'browserify', 'babel'], ['concatScripts'], ['htmlReplace'], ['copy'], ['restoreEnv']));
gulp.task('deploy', ['styles', 'copy', 'browserify', 'babel']);