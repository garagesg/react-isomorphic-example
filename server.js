var express = require('express');
var compression = require('compression');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var auth = require('basic-auth');
var favicon = require('serve-favicon');

var swig  = require('swig');
var React = require('react');
var Router = require('react-router');
var routes = require('./app/routes');
var reactRouter = require('./react-router-middleware')

var app = express();
var fs = require('fs');

app.use(compression());
app.set('port', process.env.PORT || 3000);
app.set('view engine', 'html')
app.use(logger('dev'));
app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/',express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {

  // CORS
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3001');
  res.setHeader('Access-Control-Allow-Methods', 'GET');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  if(req.url.indexOf("/img/") === 0) {
      res.setHeader("Cache-Control", "public, max-age=0"); // 4 days
      res.setHeader("Expires", new Date(Date.now() + 345600000).toUTCString());
  }

	next();

});

app.use(reactRouter(routes));

app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});
