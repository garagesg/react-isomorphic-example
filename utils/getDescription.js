'use strict';

var assign = require('react/lib/Object.assign')

function getDescription(routes, params, props, options) {
  options = assign({reverse: true, join: ' · ', defaultTitle: '(untitled)'}, options)
  var description;
  routes.forEach(route => {
    var handler = route.handler
    if (handler.description) {
      description = handler.description;
    }
    else {
      description = "";
    }
  })

  return description;
}

module.exports = getDescription