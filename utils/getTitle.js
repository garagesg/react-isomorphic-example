'use strict';

var assign = require('react/lib/Object.assign')

function getTitle(routes, params, props, options) {
  options = assign({
    reverse: true, 
    join: ' | ', 
    defaultTitle: 'Default Title',
    shortTitle: 'Default Short Title'}, 
    options)
  var titleParts = [];
  var isHome = false;

  routes.forEach(route => {
    var handler = route.handler;

    switch(route.name){
      case "app":
        return;
      case "home":
        isHome = true;
        return;
    }

    if (handler.title) {
      titleParts[0] = handler.title;
      // titleParts.push(handler.title)
      return;
    }
    // else if (handler.getTitle) {
    //   titleParts.push(handler.getTitle(props, params))
    // }
    else if(props.data.title){
      titleParts[0] = props.data.title;
      // titleParts.push(props.data.title);
      return;
    }
  })

  if (options.reverse) {
    titleParts.reverse()
  }

  titleParts.push(options.shortTitle)

  if(isHome){
    return options.defaultTitle
  }else{
    return titleParts.join(options.join) || options.defaultTitle
  }
}

module.exports = getTitle